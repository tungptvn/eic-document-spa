import 'isomorphic-fetch';
import { Aurelia, PLATFORM } from 'aurelia-framework';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';
declare const IS_DEV_BUILD: boolean; // The value is supplied by Webpack during the build

export function configure(aurelia: Aurelia) {
    aurelia.use
        .standardConfiguration()
        .plugin( PLATFORM.moduleName('aurelia-validation'))
        .plugin( PLATFORM.moduleName('aurelia-configuration'))
        .plugin( PLATFORM.moduleName('aurelia-dialog'), config => {
            config.useDefaults();
            config.settings.lock = false;
            config.settings.centerHorizontalOnly = true;
            config.settings.startingZIndex = 999;
            config.settings.enableEscClose = true;
        })
        .feature(PLATFORM.moduleName('app/common/index'))

    if (IS_DEV_BUILD) {
        aurelia.use.developmentLogging();
    }

    aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app/components/app/app')));
}
