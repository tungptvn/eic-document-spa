import { Router } from 'aurelia-router';
import { autoinject } from "aurelia-dependency-injection";
@autoinject()
export class Login {
    constructor(private router: Router) {

    }
    login() {
        this.router.navigateToRoute('admin');
    }
}