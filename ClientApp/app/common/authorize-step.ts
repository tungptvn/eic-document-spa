import { getLogger } from 'aurelia-logging';
import { AuthenService } from './authen-service';
import { inject } from 'aurelia-dependency-injection';
import { Redirect } from 'aurelia-router';
const logger = getLogger("AuthorizeStep");
@inject(AuthenService)
export class AuthorizeStep {
  constructor(private authenService: AuthenService) {}
  run(navigationInstruction, next) {
    //Dùng để check authenlicate cho từng router
    //Tạm thời đóng lại => sẽ fix sau
    if (navigationInstruction.getAllInstructions().some(i => i.config.settings.auth)) {
      var isLoggedIn = this.authenService.isAuthenticated;
      if (!isLoggedIn) {
        logger.debug("Not Authenticated")
        return next.cancel(new Redirect('login'));
      }
    }

    return next();
  }
}
