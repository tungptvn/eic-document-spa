import { DialogController, DialogService } from 'aurelia-dialog';
import { ValidationControllerFactory, ValidationController } from "aurelia-validation";
import { PLATFORM } from "aurelia-pal";
import { inject } from 'aurelia-framework';
import { BootstrapFormRenderer } from './../../bootstrap-form-renderer';

import $ from 'jquery';

@inject(DialogController, ValidationControllerFactory, DialogService)
export class DialogConfirm {
    validationcontroller: ValidationController;

    constructor(private dialogcontroller: DialogController, private controllerFactory,
        private dialogService: DialogService) {
        this.validationcontroller = controllerFactory.createForCurrentScope();
        this.validationcontroller.addRenderer(new BootstrapFormRenderer());
    }
    onReady() {
    }
    activate() {
    }
    save() {
        this.validationcontroller.validate().then((result) => {
            if (result.valid) {
                //this.dialogcontroller.ok(this.doiTacDto);
            }
        })

    }
    close() {
        this.dialogcontroller.cancel();
    }
}
