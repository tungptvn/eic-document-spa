﻿import { customElement, bindable, bindingMode, inject, child } from 'aurelia-framework';

import { IWebApiServices, IEmptyConstruct, IResultMessage, ResultStatus, IFilter } from './../../interfaces/index';
import { WebApiServices } from './../../services/index';

import $ from "jquery";
import 'select2';

@customElement("select2")
@inject(WebApiServices)
export class DropdownControl {
  @bindable entity: IEmptyConstruct;
  @bindable action: string = '';
  @bindable placeholder: string = "Vui lòng chọn ...";
  @bindable filters: IFilter[] = [];
  @bindable displayFieldName: string = 'name';
  @bindable valueFieldName: string = 'value';
  @bindable multiple: boolean = false;
  @bindable showTag: boolean = false;
  @bindable allowedLoadDataOnInit: boolean = true;
  @bindable minimumResultsForSearch: number = 0;
  @bindable({ defaultBindingMode: bindingMode.twoWay }) value: any;
  @bindable selected: Function;

  private select2Control: any;

  private static controlId: number = 1;
  private id: string = "";

  constructor(private service: IWebApiServices) {
    this.id = `select2${DropdownControl.controlId++}`;
  }

  attached() {
    this.loadData();
  }

  detached() {
    this.destroy();
  }

  public async loadData() {
    let that = this;
    this.destroy();
    setTimeout(_ => {
      that.service.getEntityAsync(that.entity, that.action).then((data: any) => {
        that.select2Control = $("#" + that.id).select2({
          data: data.map((item: any) => {
            return { id: item[that.valueFieldName], text: item[that.displayFieldName] };
          }),
          //minimumResultsForSearch: that.minimumResultsForSearch > 0 ? that.minimumResultsForSearch : Infinity,
          multiple: that.multiple,
          //placeholder: that.placeholder,
          //tags: that.showTag
        });
        that.select2Control.on("select2:select", function (e: any) {
          that.selectedItem(that.select2Control.val());
        });
        that.select2Control.on("select2:unselect", function (e: any) {
          that.selectedItem(that.select2Control.val());
        });
        if (that.value) that.select2Control.val(that.value).trigger('change');
        that.selectedItem(that.select2Control.val());
      });
    }, 1);
  }

  // private initDropdownEmpty() {
  //   let that = this;
  //   if (!that.select2Control) {
  //     that.select2Control = $("#" + that.id).select2({
  //       data: [],
  //       minimumResultsForSearch: that.minimumResultsForSearch > 0 ? that.minimumResultsForSearch : Infinity,
  //       multiple: that.multiple,
  //       placeholder: that.placeholder,
  //       tags: that.showTag
  //     });
  //   }
  // }

  private selectedItem(value: any) {
    this.value = value;
    //this.selected(value);
  }

  private destroy() {
    let that = this;
    if (that.select2Control) {
      that.select2Control.empty();
      that.select2Control.select2("destroy");
      that.select2Control = null;
    }
  }
}
