﻿import { customElement, bindable, bindingMode, inject, child, observable } from 'aurelia-framework';
import { PaginatorControl } from './paginator.control';
import { IEmptyConstruct, IDataStructure, IGridHeader, IResultMessage, FieldType, IFilter, ButtonLocation, IGridButton, IWebApiServices } from './../../interfaces/index';
import { WebApiServices } from './../../services/index';

@customElement("grid")
@inject(WebApiServices)
export class GridViewControl {
  @bindable entity;
  entityChanged(entity: IEmptyConstruct) {
    this.entityBase = new entity();
    this.emptyConstruct = entity;
  }

  @bindable useDataTest: boolean = false;
  @bindable dataBinding: IDataStructure[];
  @bindable actionName: string = "";
  @bindable defaultImageUrl: string = "./assets/images/default-image.png";
  @bindable useCheckbox = true;
  @bindable filters: IFilter[] = [];
  @bindable pageSize: number = 10;

  @bindable actionEvent;
  @bindable deleteEvent;

  private paginator: PaginatorControl;
  private emptyConstruct: IEmptyConstruct;
  private entityBase: any;
  private sources: any;
  private totalRecord: number = 0;
  private loading: boolean = true;
  private pageIndex: number = 1;
  private pager: any = {};

  private static controlId: number = 1;
  private id: string = "";

  constructor(private service: IWebApiServices) {
    this.id = `gridview${GridViewControl.controlId++}`;
  }

  attached() {
    this.loadData();
  }

  public loadData(){
    if(this.useDataTest) this.loadDataSimple();
    else if(this.dataBinding) this.loadDataBinding();
    else this.loadDataApi();
  }

  private loadDataSimple() {
    setTimeout(_ => {
      var that = this;
      that.loading = true;
      that.sources = that.entityBase.dataTestGridview();
      that.paginator.setTotalRecords(that.entityBase.totalRecords(), this.pageIndex);
      that.loading = false;
    }, 1);
  }

  private loadDataBinding() {
    setTimeout(_ => {
      var that = this;
      that.loading = true;
      that.sources = that.dataBinding;
      that.paginator.setTotalRecords(that.dataBinding.length, this.pageIndex);
      that.loading = false;
    }, 1);
  }

  private loadDataApi() {
    setTimeout(_ => {
      var that = this;
      that.loading = true;
      that.service.getAllAsync(this.emptyConstruct, this.actionName, this.pageIndex, this.pageSize, this.filters).then((data: IResultMessage) => {
        that.sources = data.dataResult.map((item: any) => {
          return { id: item.id, checked: false, data: item };
        });
        that.paginator.setTotalRecords(data.totalCount, this.pageIndex);
        that.loading = false;
      }).catch((error: any) => {
        that.loading = false;
      });
    }, 1);
  }

  public getRowChecked(): any[] {
    return this.sources.filter((item: any) => item.checked == true);
  }

  public checkAll(value: any) {
    if (this.sources) this.sources.forEach((item: any) => { item.checked = value; });
  }

  private onSelectPage(pageIndex: number) {
    this.pageIndex = pageIndex;
    if(this.dataBinding) this.loadDataBinding();
    else this.loadDataApi();
  }

  private onButtonEvent(item: IDataStructure, button: IGridButton) {
    this.actionEvent({ 'data': item, 'button': button });
  }

  private isButtonLeftTop(button: IGridButton) {
    return button.Location == ButtonLocation.LeftTop;
  }
  private isButtonRightTop(button: IGridButton) {
    return button.Location == ButtonLocation.RightTop;
  }
  private isButtonInGrid(button: IGridButton) {
    return button.Location == ButtonLocation.InGrid;
  }

  private hasButtonInGrid() {
    return this.entityBase.getButtons()
      .find((button: IGridButton) => button.Location === ButtonLocation.InGrid) != null;
  }

  private rowCheck($event: any, item: IDataStructure) {
    console.log($event);
  }

  private isFieldImage(field: IGridHeader) {
    return field.FieldType === FieldType.Image;
  }

  private isFieldText(field: IGridHeader) {
    return field.FieldType === FieldType.Text;
  }

  private isFieldNumber(field: IGridHeader) {
    return field.FieldType === FieldType.Number;
  }

  private isFieldDate(field: IGridHeader) {
    return field.FieldType === FieldType.Date;
  }

  private isFieldCurrencyTag(field: IGridHeader) {
    return field.FieldType === FieldType.Currency;
  }

  private isFieldStatusRecord(field: IGridHeader) {
    return field.FieldType === FieldType.Status;
  }

  private isFieldButton(header: IGridHeader, button: IGridButton) {
    return button.HeaderName == header.Name && header.FieldType === FieldType.Button;
  }
}
