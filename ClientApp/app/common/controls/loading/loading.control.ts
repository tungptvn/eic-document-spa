﻿import { customElement, bindable, bindingMode, inject, child } from 'aurelia-framework';

@customElement("loading")
export class LoadingControl {
    @bindable loading: boolean = false;
}
