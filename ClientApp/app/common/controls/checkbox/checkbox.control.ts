﻿//http://icheck.fronteed.com/
import { customElement, bindable, bindingMode, inject, child } from 'aurelia-framework';
import { PLATFORM } from "aurelia-pal";

import $ from "jquery";

@customElement("checkbox")
export class NumberControl {
  @bindable({ defaultBindingMode: bindingMode.twoWay }) value: boolean;
  @bindable text: string;
  @bindable checkedEvent: Function = (_: any) => { };

  private control: any;
  private static controlId: number = 1;
  private id: string = "";

  constructor() {
    this.id = `checkbox${NumberControl.controlId++}`;
  }

  attached() {
    this.init();
  }

  detached() {
    this.control.iCheck('destroy');
  }

  valueChanged(value: boolean) {
    if (this.control) this.control.iCheck(value == true ? 'check' : 'uncheck');
  }

  private init() {
    let that = this;
    if (!that.control) {
      that.control = PLATFORM.global.$("#" + that.id).iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
      that.control.on('ifChecked', function (event: any) {
        that.onChanged(true);
      });
      that.control.on('ifUnchecked', function (event: any) {
        that.onChanged(false);
      });
      if (that.value) that.control.iCheck('check');
    }
  }

  private onChanged(value: boolean) {
    this.value = value;
    this.checkedEvent(value);
  }
}
