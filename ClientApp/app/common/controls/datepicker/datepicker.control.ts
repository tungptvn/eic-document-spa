﻿//https://bootstrap-datepicker.readthedocs.io/en/latest/
import { customElement, bindable, bindingMode, inject, child } from 'aurelia-framework';
import { PLATFORM } from "aurelia-pal";

import $ from "jquery";
import * as moment from 'moment';

@customElement("datepicker")
export class DatepickerControl {
  @bindable({ defaultBindingMode: bindingMode.twoWay }) value: any;
  @bindable formatDate: string = "dd/mm/yyyy";

  private dateControl: any;

  private static controlId: number = 1;
  private id: string = "";

  constructor() {
    this.id = `datepicker${DatepickerControl.controlId++}`;
  }

  attached() {
    this.init();
  }

  detached() {
    this.dateControl.datepicker('destroy');
  }

  private init() {
    let that = this;
    if (!that.dateControl) {
      that.dateControl = PLATFORM.global.$("#" + that.id).datepicker({
         language: 'vi',
        format: that.formatDate,
      }).on('changeDate', function (e: any) {
        that.selectedItem(e.date);
      });
      if (this.value) this.dateControl.datepicker('setDate', this.value);
    }
  }

  private selectedItem(value: any) {
    this.value = value;
  }
}
