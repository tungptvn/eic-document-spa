﻿//https://github.com/autoNumeric/autoNumeric
import { customElement, bindable, bindingMode, inject, child } from 'aurelia-framework';
import { PLATFORM } from "aurelia-pal";

import $ from "jquery";

@customElement("number")
export class NumberControl {
  @bindable({ defaultBindingMode: bindingMode.twoWay }) value: number = 0;
  @bindable max: number = 99999999999999999.99;
  @bindable min: number = -99999999999999999.99;

  private control: any;
  private static controlId: number = 1;
  private id: string = "";

  constructor() {
    this.id = `number${NumberControl.controlId++}`;
  }

  attached() {
    this.control = PLATFORM.global.$("#" + this.id).autoNumeric('init', {
      vMax: this.max,
      vMin: this.min,
    });
    if(this.value) this.control.autoNumeric('set', this.value);
  }

  detached() {
    this.control.autoNumeric('destroy');
  }

  private onChanged() {
    if (this.control) {
      var result = this.control.autoNumeric('get');
      if(result) this.value = parseFloat(result);
      else this.value = undefined;
    }
  }
}
