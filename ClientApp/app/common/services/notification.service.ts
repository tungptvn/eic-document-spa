import { inject } from 'aurelia-framework';
import { PLATFORM } from "aurelia-pal";

import $ from "jquery";

export class NotificationService {

  /**
   * Gồm các style sau: bar, flip, circle, simple
   * 
   * @type {string}
   * @memberof NotificationService
   */
  public style: string = 'bar';

  /**
   * Gom cac vi tri sau: top, left, right, bottom
   * 
   * @type {string}
   * @memberof NotificationService
   */
  public position: string = 'top';
  public timeout: number = 3000;

  private info: string = 'info';
  private warning: string = 'warning';
  private success: string = 'success';
  private danger: string = 'danger';
  private default: string = 'default';

  constructor() { }

  public showAddSuccess() {
    this.showSuccess("Thêm mới dữ liệu thành công");
  }

  public showEditSuccess() {
    this.showSuccess("Cập nhật dữ liệu thành công");
  }

  public showDeleteSuccess() {
    this.showSuccess("Hủy dữ liệu thành công");
  }

  public showErrorUpdateEntity() {
    this.showError("Cập nhật dữ liệu thất bại");
  }

  /**
   * showSuccess
   */
  public showSuccess(message: string) {
    PLATFORM.global.$('body').pgNotification({ 
      style: this.style, 
      message: message, 
      position: this.position, 
      timeout: this.timeout, 
      type: this.success }).show();
  }

  /**
   * showError
   */
  public showError(message: string) {
    PLATFORM.global.$('body').pgNotification({ 
      style: this.style, 
      message: message, 
      position: this.position, 
      timeout: this.timeout, 
      type: this.danger }).show();
  }

  /**
   * showWarning
   */
  public showWarning(message: string) {
    PLATFORM.global.$('body').pgNotification({ 
      style: this.style, 
      message: message, 
      position: this.position, 
      timeout: this.timeout, 
      type: this.warning }).show();
  }

  /**
   * showInfo
   */
  public showInfo(message: string) {
    PLATFORM.global.$('body').pgNotification({ 
      style: this.style, 
      message: message, 
      position: this.position, 
      timeout: this.timeout, 
      type: this.info }).show();
  }

  /**
   * showDefault
   */
  public showDefault(message: string) {
    PLATFORM.global.$('body').pgNotification({ 
      style: this.style, 
      message: message, 
      position: this.position, 
      timeout: this.timeout, 
      type: this.default }).show();
  }
}
