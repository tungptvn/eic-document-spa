﻿import { AureliaConfiguration } from 'aurelia-configuration';
import { autoinject, inject } from "aurelia-framework";
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';

import { AppSetting } from './../app-setting';
import { IWebApiServices, IDataStructure, IEmptyConstruct, IFilter, IResultMessage } from './../interfaces/index';

import * as firebase from 'firebase';
import * as lodash from 'lodash';

@inject(HttpClient, AureliaConfiguration)
export class LocalApiServices implements IWebApiServices {

  private fbService = firebase.database();

  constructor(private http: HttpClient,
    private config: AureliaConfiguration) {
  }

  public async getAllAsync(dataStructure: IEmptyConstruct, actionName?: string, pageIndex?: number, pageSize?: number, filters?: IFilter[]): Promise<any> {
    var that = this;

    var url = `${this.getEntityName(dataStructure)}`;
    if (actionName) url += "/" + actionName;

    return new Promise((resolve, reject) => {
      this.fbService.ref(url).once('value').then(function (snapshot) {
        let result: any[] = [];
        snapshot.forEach(function (childSnapshot) {
          var childKey = childSnapshot.key;
          var childData = childSnapshot.val();
          result.push(lodash.assignIn({ id: childKey }, childData));
        });
        resolve(result);
      }).catch(err => reject(err));
    });
  }

  public async getSingleAsync(dataStructure: IEmptyConstruct, id: number): Promise<any> {
    var that = this;

    var url = `${this.getEntityName(dataStructure)}`;
    if (id) url += "/" + id;

    return new Promise((resolve, reject) => {
      this.fbService.ref(url).once('value').then(snapshot => {
        resolve(snapshot.val());
      }).catch(err => reject(err));
    });
  }

  public async addEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any> {
    var that = this;

    var url = `${this.getEntityName(dataStructure)}`;
    if (actionName) url += "/" + actionName;

    return new Promise((resolve, reject) => {
      var itemLast = this.fbService.ref(url).limitToLast(1);
      resolve(itemLast);
      // this.getDoiTacs().then(res => {
      //     let maxMaDoiTac: number = .maxBy(res, function (o) { return o['id']; }).id;
      //     doiTac.id = +maxMaDoiTac + 1;
      //     this.db.ref('/partner/' + doiTac.id).set(doiTac, (error?) => {
      //         if (error) {
      //             reject(new Error('firebase errors'));
      //         }
      //     }).then((res) => {
      //         resolve(res);
      //     })
      // })
    })
  }

  public async updateEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any> {
    var that = this;

    var url = `${this.getEntityName(dataStructure)}`;
    if (actionName) url += "/" + actionName;
    url += "/" + entity.id;

    return new Promise((resolve, reject) => {
      this.fbService.ref(url).set(entity, (error?) => {
        if (error) {
          reject(new Error('firebase errors'));
        }
      }).then((res) => {
        resolve(res);
      });
    })
  }

  public async deleteEntityAsync(dataStructure: IEmptyConstruct, id: number, actionName?: string): Promise<any> {
    var that = this;

    var url = `${this.getEntityName(dataStructure)}`;
    if (id) url += "/" + id;

    return new Promise((resolve, reject) => {
      let doiTacBiXoa = this.fbService.ref(url).remove().then(res => {
        resolve(res)
      }).catch(err => { reject(err) });
    })
  }
  public async deleteEntityAllAsync(dataStructure: IEmptyConstruct, id: string, actionName?: string): Promise<any>{
    var that = this;

    return null
  }
  public async patchEntityAsync(dataStructure: IEmptyConstruct, id: number, actionName: string): Promise<any> {
    var that = this;

    return null
  }

  public async getEntityAsync(dataStructure: IEmptyConstruct, actionName?: string, filters?: IFilter[]): Promise<IResultMessage> {
    var that = this;

    return null;
  }

  public async postEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<IResultMessage> {
    var that = this;

    return null;
  }

  /**
   * get Entity from instances dummy
   * @param DataStructure Type of data
   * @param entity Data entity
   * @returns IDataStructure
   */
  private fromRawEntity(dataStructure: IEmptyConstruct, entity: IDataStructure): IDataStructure {
    let res: any = new dataStructure();
    (res as IDataStructure).setModelData(entity);
    return res;
  }

  /**
   * Get Module Name
   * @param DataStructure Type of data
   * @returns ModuleName()
   */
  private getModuleName(DataStructure: IEmptyConstruct): string {
    let res: any = new DataStructure();
    return (res as IDataStructure).getModuleName();
  }

  /**
  * Get Entity Name
  * @param DataStructure Type of data
  * @returns EntityName()
  */
  private getEntityName(DataStructure: IEmptyConstruct): string {
    let res: any = new DataStructure();
    return (res as IDataStructure).getEntityName();
  }
}
