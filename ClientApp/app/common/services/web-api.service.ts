﻿import { AureliaConfiguration } from 'aurelia-configuration';
import { autoinject, inject } from "aurelia-framework";
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';

import { STORAGE } from './../../common/storage';
import { AppSetting } from './../app-setting';
import { IWebApiServices, IDataStructure, IEmptyConstruct, IFilter, IResultMessage } from './../interfaces/index';

@inject(HttpClient, STORAGE, AureliaConfiguration)
export class WebApiServices implements IWebApiServices {

  constructor(private http: HttpClient,
    private storage: STORAGE,
    private config: AureliaConfiguration) {
    this.http.configure(x => {
      x.withBaseUrl(config.get('api.endpoint'));
      x.withHeader('Accept', 'application/json');
      x.withHeader('Content-Type', 'application/json');
      x.withHeader('API_KEY', config.get('api.API_KEY'));
      //x.withHeader('USER_SESSION_TOKEN', storage.get(STORAGE.tokenKey));
    });
  }

  public async getAllAsync(dataStructure: IEmptyConstruct, actionName?: string, pageIndex?: number, pageSize?: number, filters?: IFilter[]): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (actionName) url += "/" + actionName;
    url += pageIndex ? "?page=" + pageIndex.toString() : "&pageIndex=1";
    url += pageIndex ? "&pageItems=" + pageSize.toString() : "&pageItems=20";
    for (let filter of filters) {
      url += `&${filter.FieldName}=${filter.Value}`;
    }

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asGet().send();
      return { dataResult: response.content || '', totalCount: parseInt(response.headers.get('X-Pagination-Total-Count')) | 0 };
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async getSingleAsync(dataStructure: IEmptyConstruct, id: number): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (id) url += "/" + id;

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asGet().send();
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async addEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (actionName) url += "/" + actionName;

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asPost().withContent(entity)
        .send();
      //let response = await this.http.post(url, JSON.stringify(entity));
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async updateEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (actionName) url += "/" + actionName;

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asPut().withContent(entity)
        .send();
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async deleteEntityAsync(dataStructure: IEmptyConstruct, id: number, actionName?: string): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (id) url += "/" + id;
    if (actionName) url += "/" + actionName;

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asDelete()
        .send();
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async deleteEntityAllAsync(dataStructure: IEmptyConstruct, id: string, actionName?: string): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (actionName) url += "/" + actionName;
    if (id) url += "/" + id;

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asDelete()
        .send();
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async patchEntityAsync(dataStructure: IEmptyConstruct, id: number, actionName: string): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (id) url += "/" + id;
    if (actionName) url += "/" + actionName;

    try {
      let response = await this.http.createRequest(url)
        .withHeader('USER_SESSION_TOKEN', this.storage.get(STORAGE.tokenKey))
        .asPatch()
        .send();
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async getEntityAsync(dataStructure: IEmptyConstruct, actionName?: string, filters?: IFilter[]): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (actionName) url += "/" + actionName;
    url += filters ? "?filters=" + JSON.stringify(filters) : "?filters=[]";

    try {
      let response = await this.http.get(url);
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public async postEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any> {
    var that = this;

    var url = that.getUrlFormModel(dataStructure);
    if (actionName) url += "/" + actionName;

    try {
      let response = await this.http.post(url, JSON.stringify(entity));
      return response.content || '';
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  public getUrlFormModel(dataStructure: IEmptyConstruct): string {
    var url = "";
    if (dataStructure) {
      if (this.getModuleName(dataStructure)) url += '/' + this.getModuleName(dataStructure);
      if (this.getEntityName(dataStructure)) url += '/' + this.getEntityName(dataStructure);
    }

    return url;
  }

  /**
   * get Entity from instances dummy
   * @param DataStructure Type of data
   * @param entity Data entity
   * @returns IDataStructure
   */
  private fromRawEntity(dataStructure: IEmptyConstruct, entity: IDataStructure): IDataStructure {
    let res: any = new dataStructure();
    (res as IDataStructure).setModelData(entity);
    return res;
  }

  /**
   * Get Module Name
   * @param DataStructure Type of data
   * @returns ModuleName()
   */
  private getModuleName(DataStructure: IEmptyConstruct): string {
    let res: any = new DataStructure();
    return (res as IDataStructure).getModuleName();
  }

  /**
  * Get Entity Name
  * @param DataStructure Type of data
  * @returns EntityName()
  */
  private getEntityName(DataStructure: IEmptyConstruct): string {
    let res: any = new DataStructure();
    return (res as IDataStructure).getEntityName();
  }
}
