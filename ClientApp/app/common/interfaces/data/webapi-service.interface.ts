﻿import { IDataStructure, IEmptyConstruct, IFilter, IResultMessage } from './../index';
export interface IWebApiServices {
  getAllAsync(dataStructure: IEmptyConstruct, actionName?: string, pageIndex?: number, pageSize?: number, filters?: IFilter[]): Promise<any>;
  getSingleAsync(dataStructure: IEmptyConstruct, id: number): Promise<any>;
  addEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any>;
  updateEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any>;
  deleteEntityAsync(dataStructure: IEmptyConstruct, id: number, actionName?: string): Promise<any>;
  deleteEntityAllAsync(dataStructure: IEmptyConstruct, id: string, actionName?: string): Promise<any>;
  patchEntityAsync(dataStructure: IEmptyConstruct, id: number, actionName: string): Promise<any>;
  getEntityAsync(dataStructure: IEmptyConstruct, actionName?: string, filters?: IFilter[]): Promise<any>;
  postEntityAsync(dataStructure: IEmptyConstruct, entity: IDataStructure, actionName?: string): Promise<any>;
}
