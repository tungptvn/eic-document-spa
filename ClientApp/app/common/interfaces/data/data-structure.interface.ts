﻿/**
  * Data Structure interfaces
  * @desc Define some basic functions to work with Entity
*/
export interface IDataStructure {
    id: any; 
    setModelData(modelData: IDataStructure): void;

    getModuleName(): string;
    getEntityName(): string;
}
