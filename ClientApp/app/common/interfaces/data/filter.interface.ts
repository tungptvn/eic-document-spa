﻿export interface IFilter {
    FieldName: string;
    Operator: string;
    Value: any;
}