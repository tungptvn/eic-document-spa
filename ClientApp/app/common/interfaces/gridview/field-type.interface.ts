﻿export enum FieldType{
    Text,
    Number,
    Date,
    Image,
    Button,
    Currency,
    Status,
    LoaiGiayTo,
    HieuLuc,
    DauHieuDangNgo
}
