﻿export enum ButtonActionType {
    Add,
    Edit,
    Delete,
    View,
    DeleteList,
    Print,
    Upload,
    Download,
    ExportExcel,
    ExportPdf,
    Approved,
    Canceled
}
export enum ButtonLocation {
    LeftTop,
    RightTop,
    InGrid,
    Header
}
export interface IGridButton {
    Title: string;
    Class?: string;
    Icon?: string;
    Action?: ButtonActionType;
    Location?: ButtonLocation;
    Diabled?: boolean;
    Link?: string;
    HeaderName?: string;
    PublicEvent?: boolean;
}
