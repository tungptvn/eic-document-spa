﻿import { FieldType } from './../index';

export interface IGridHeader {
    Name: string;
    Title: string;
    Width?: string;
    FieldType?: FieldType;
}