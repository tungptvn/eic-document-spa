import { getLogger } from 'aurelia-logging';
import { buildQueryString, join } from 'aurelia-path';
import { AureliaConfiguration } from 'aurelia-configuration';
import { Router } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { inject } from 'aurelia-framework';
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';

import axios from 'axios';
import { STORAGE } from './../common/storage';
import { IEmptyConstruct, IDataStructure } from './interfaces/index';

const logger = getLogger("Authentication service");
@inject(STORAGE, Router, HttpClient, AureliaConfiguration)
export class AuthenService {

    private userLogin: any;

    constructor(private storage: STORAGE,
        private router: Router,
        private http: HttpClient,
        private config: AureliaConfiguration) {

        this.http.configure(x => {
            x.withBaseUrl(config.get('api.endpoint'));
            x.withHeader('Accept', 'application/json');
            x.withHeader('Content-Type', 'application/json');
            //x.withHeader('API_KEY', config.get('api.API_KEY'));
        });
    }

    get isAuthenticated(): boolean { return this.storage.get(STORAGE.tokenKey) != null; }

    get getToken() { return this.storage.get(STORAGE.tokenKey); }

    get userInfo(): any { return this.storage.get(STORAGE.userInfoKey); }

    logout() {
        this.storage.remove(STORAGE.tokenKey);
        this.storage.remove(STORAGE.userInfoKey);
        this.router.navigateToRoute('login');
    }

    login(dataStructure: IEmptyConstruct, user: IDataStructure): Promise<any> {
        let that = this;
        var url = that.getUrlFormModel(dataStructure);
        return new Promise((resolve, reject) => {
            that.http.post(url, JSON.stringify(user)).then((res: HttpResponseMessage) => {
                that.userLogin = JSON.parse(res.response);
                that.storage.set(STORAGE.tokenKey, that.userLogin.loginToken);
                that.storage.set(STORAGE.userInfoKey, that.userLogin);
                resolve(that.userLogin);
            })
                .catch((error: any) => {
                    console.log(error);
                });
        });
    }

    public getUrlFormModel(dataStructure: IEmptyConstruct): string {
        var url = this.config.get('api.endpoint');
        if (dataStructure) {
            if (this.getModuleName(dataStructure)) url += '/' + this.getModuleName(dataStructure);
            if (this.getEntityName(dataStructure)) url += '/' + this.getEntityName(dataStructure);
        }

        return url;
    }

    /**
     * Get Module Name
     * @param DataStructure Type of data
     * @returns ModuleName()
     */
    private getModuleName(DataStructure: IEmptyConstruct): string {
        let res: any = new DataStructure();
        return (res as IDataStructure).getModuleName();
    }

    /**
    * Get Entity Name
    * @param DataStructure Type of data
    * @returns EntityName()
    */
    private getEntityName(DataStructure: IEmptyConstruct): string {
        let res: any = new DataStructure();
        return (res as IDataStructure).getEntityName();
    }
}
