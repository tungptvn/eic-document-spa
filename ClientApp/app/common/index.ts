import { PLATFORM } from 'aurelia-pal';
export * from './interfaces/index';
export * from './services/index';
export * from './app-setting';
export * from './contants/common.contant';

import { FrameworkConfiguration } from 'aurelia-framework';

export function configure(config: FrameworkConfiguration) {
  config.globalResources([
    PLATFORM.moduleName("./controls/loading/loading.control"),
    PLATFORM.moduleName("./controls/checkbox/checkbox.control"),
    PLATFORM.moduleName("./controls/datepicker/datepicker.control"),
    PLATFORM.moduleName("./controls/dropdown/dropdown.control"),
    PLATFORM.moduleName("./controls/number/number.control"),
    PLATFORM.moduleName("./controls/gridview/gridview.control"),
    PLATFORM.moduleName("./controls/gridview/paginator.control"),
    PLATFORM.moduleName("./converters/format-currency"),
    PLATFORM.moduleName("./converters/format-date"),
    PLATFORM.moduleName("./converters/currency-tag"),
    PLATFORM.moduleName("./converters/status-record"),
  ]);
}
