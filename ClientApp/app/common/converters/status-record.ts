﻿import { StatusRecord } from '../interfaces/common/status.interface';
export class StatusRecordValueConverter {
  toView(value) {
    switch (value) {
      case StatusRecord.NEW:
        return `<span class="label label-default" style="margin: 0 2px;">Chưa duyệt</span>`;
      case StatusRecord.APPROVED:
        return `<span class="label label-success" style="margin: 0 2px;">Đã duyệt</span>`;
      case StatusRecord.CANCELED:
        return `<span class="label label-danger" style="margin: 0 2px;">Từ chối</span>`;
    }
    return "";
  }
}
