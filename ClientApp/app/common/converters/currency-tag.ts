﻿export class CurrencyTagValueConverter {
  toView(value) {
    var result = "";
    if (value) {
      if (typeof value === "string") {
        result += `<span class="label label-default" style="margin: 0 2px;">${value}</span>`;
      }
      else {
        value.forEach(element => {
          result += `<span class="label label-default" style="margin: 0 2px;">${element}</span>`;
        });
      }
    }
    return result;
  }
}
