# EIC ASPNET Core Aurelia

## Hướng dẫn cài đặt
### Restore dotnet packages xử dụng [.net core cli](https://docs.microsoft.com/en-us/dotnet/core/tools/) 
```bash
dotnet restore
```
### Cài đặt các packages aurelia

```bash
npm install
```

## Chạy ứng dụng

```bash
dotnet run
```